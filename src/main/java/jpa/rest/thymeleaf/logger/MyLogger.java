package jpa.rest.thymeleaf.logger;

public interface MyLogger {

    void text(String text, int space);

    void text(Object o, int space);

    void space();
}
