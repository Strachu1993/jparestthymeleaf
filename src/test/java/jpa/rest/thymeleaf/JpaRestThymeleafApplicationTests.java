package jpa.rest.thymeleaf;

import jpa.rest.thymeleaf.DAO.model.Department;
import jpa.rest.thymeleaf.DAO.model.Payment;
import jpa.rest.thymeleaf.DAO.model.User;
import jpa.rest.thymeleaf.DAO.repository.DepartmentRepository;
import jpa.rest.thymeleaf.DAO.repository.PaymentRepository;
import jpa.rest.thymeleaf.DAO.repository.UserRepository;
import jpa.rest.thymeleaf.metda.data.AnnotationCounterClass;
import jpa.rest.thymeleaf.services.JpaService;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import java.time.LocalDate;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

@RunWith(SpringRunner.class)
@SpringBootTest
public class JpaRestThymeleafApplicationTests {

	@Autowired
	private UserRepository userRepository;

	@Autowired
	private DepartmentRepository departmentRepository;

	@Autowired
	private PaymentRepository paymentRepository;

	@Autowired
	private JpaService jpaService;

	private static boolean IS_FIRST_TIME = false;
	private static int METHOD_COUNT_WITH_ANNOTATIONS;
	private static int VISITED_METHODS_COUNT = 0;

	@Before
	public void setUpClass() {
		if(!IS_FIRST_TIME){
			userRepository.save(User.createUser("TestDesc", "TestFirstName", "TestLastName", "TestPass", "TestUserName"));
			departmentRepository.save(Department.createDepartment("TestDddress", "TestName", "TestDescription", "TestMail", "TestPhone", "TestWww"));
			paymentRepository.save(Payment.createPayment(36.75f, LocalDate.now(), 555.55f));
			jpaService.logDataFromDb();
			IS_FIRST_TIME = true;
			METHOD_COUNT_WITH_ANNOTATIONS = AnnotationCounterClass.COUNT_METHODS_WITH_ANNOTATIONS(JpaRestThymeleafApplicationTests.class, Test.class);
		}
	}

	@After
	public void tearDown(){
		if(VISITED_METHODS_COUNT != METHOD_COUNT_WITH_ANNOTATIONS - 1)
			VISITED_METHODS_COUNT++;
		else
			jpaService.logDataFromDb();
	}

	@Test
	public void editUser() throws Exception {
		long userId = userRepository.findAll()
				.stream()
				.findFirst()
				.get()
				.getId();

		User userData = User.createUser("NewDesc", "NewFirstName", "NewLastName", "NewPass", "NewUserName");
		jpaService.editUser(userId, userData);

		assertNotNull(userRepository.findByUserName("NewUserName"));
	}

	@Test
	public void addUserToDepartment(){

		User user = userRepository.findAll()
				.stream()
				.findFirst()
				.get();

		Department department = departmentRepository.findAll()
				.stream()
				.findFirst()
				.get();

		long userDepartmentsCountBefore = user.getDepartments().size();
		long departmentUsersCountBefore = department.getUsers().size();

		jpaService.addUserToDepartment(user.getId(), department.getId());

		assertTrue(userRepository.findOne(user.getId()).getDepartments().size() > userDepartmentsCountBefore);
		assertTrue(departmentRepository.findOne(department.getId()).getUsers().size() > departmentUsersCountBefore);
	}

	@Test
	public void addPaymantToUser(){
		long userId = userRepository.findAll()
				.stream()
				.findFirst()
				.get()
				.getId();

		long paymantId = paymentRepository.findAll()
				.stream()
				.findFirst()
				.get()
				.getId();

		jpaService.addPaymantToUser(paymantId, userId);

		assertNotNull(userRepository.findOne(userId).getPayment());
	}

	@Test
	public void addUser() {
		long userCountBefore = userRepository.count();
		jpaService.createUser("Desc", "Name", "LastNAme", "Pass", "UserName");
		long userCountAfter = userRepository.count();

		assertTrue(userCountBefore != userCountAfter);
	}

	@Test
	public void addDepatment() {
		long departmentCountBefore = departmentRepository.count();
		jpaService.createDepartment("Address", "Name", "Desc", "Mail", "Phone", "Www");
		long departmentCountAfter = departmentRepository.count();

		assertTrue(departmentCountBefore != departmentCountAfter);
	}

	@Test
	public void addPaymant() {
		long paymentCountBefore = paymentRepository.count();
		jpaService.createPaymant(44.2f, LocalDate.now(), 5555.33f);
		long paymentCountAfter = paymentRepository.count();

		assertTrue(paymentCountBefore != paymentCountAfter);
	}

}
