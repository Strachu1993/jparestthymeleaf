package jpa.rest.thymeleaf;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class JpaRestThymeleafApplication {
	public static void main(String[] args) {
		SpringApplication.run(JpaRestThymeleafApplication.class, args);
	}
}
