package jpa.rest.thymeleaf.DAO.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.sun.istack.internal.NotNull;
import lombok.*;

import javax.annotation.PostConstruct;
import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@ToString(exclude = {"users"})
@Setter
@Getter
@NoArgsConstructor
@EqualsAndHashCode(exclude = {"users"})
@Entity
@Table(name = "Department")
public class Department {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @NotNull
    @Setter(AccessLevel.PROTECTED)
    private long id;

    private String address;

    @NotNull
    private String departmentName;

    private String description;

    private String mainMail;

    private String mainPhone;

    private String mainWww;

    @JsonIgnore
    @ManyToMany(cascade = {CascadeType.MERGE}, fetch=FetchType.EAGER)
    private List<User> users = new ArrayList<>();

    public static final Department createDepartment(String address, String departmentName, String description, String mainMail, String mainPhone, String mainWww){
        Department department = new Department();
        department.setAddress(address);
        department.setDepartmentName(departmentName);
        department.setDescription(description);
        department.setMainMail(mainMail);
        department.setMainPhone(mainPhone);
        department.setMainWww(mainWww);
        return department;
    }

}