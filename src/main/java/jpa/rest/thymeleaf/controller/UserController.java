package jpa.rest.thymeleaf.controller;

import javassist.NotFoundException;
import jpa.rest.thymeleaf.services.BookKeepingService;
import jpa.rest.thymeleaf.services.JpaService;
import lombok.AllArgsConstructor;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;

@AllArgsConstructor
@RequestMapping(value = "/user", produces = MediaType.APPLICATION_JSON_VALUE)
@RestController
public class UserController {

    private final JpaService jpaService;
    private final BookKeepingService bookKeepingService;

    @GetMapping("getUsers")
    public List getUsers() {
        return jpaService.getUsers();
    }

    @GetMapping("annualSalary/{userName}")
    public String getAnnualSalary(@PathVariable("userName") String userName) throws NotFoundException {
        return bookKeepingService.annualSalary(jpaService.findUserByName(userName));
    }

    @GetMapping("monthlyTaxes/{departmentName}/percent")
    public String getMonthlyTaxes(@PathVariable("departmentName") String departmentName, @RequestParam(value = "percent", defaultValue = "5") int percent) {
        return bookKeepingService.monthlyTaxes(jpaService.findDepartmentByName(departmentName), percent);
    }

    void asd() {
        List<Integer> list = new ArrayList<>();

        int a;
        int b;
        int c;

    }

}
