package jpa.rest.thymeleaf.logger;

import lombok.extern.java.Log;
import org.springframework.stereotype.Service;

@Log(topic = "- - - - - - -> SERVER LOG <- - - - - - -")
@Service
public class MyLoggerImpl implements MyLogger {

    private long count = 0;
    private final String space = "      ";

    @Override
    public void text(String text, int spaceCount) {
        log.info(spaceGenerator(spaceCount) + ++count + ": " + text);
    }

    @Override
    public void text(Object o, int spaceCount) {
        log.info(spaceGenerator(spaceCount) + ++count + ": " + o.toString());
    }

    @Override
    public void space() {
        log.info("");
    }

    private String spaceGenerator(int spaceCount) {
        String s = "";
        for (int i = 0; i < spaceCount; i++)
            s += space;
        return s;
    }

}