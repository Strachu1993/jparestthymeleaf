package jpa.rest.thymeleaf.metda.data;

import java.lang.annotation.Annotation;
import java.lang.reflect.Method;

public class AnnotationCounterClass {
    
	public static int COUNT_METHODS_WITH_ANNOTATIONS(Class<?> c, Class<? extends Annotation> a) {

    	int count = 0;

        for (Method m : c.getMethods())
            if (m.isAnnotationPresent(a))
                count++;

        return count;
    }

}