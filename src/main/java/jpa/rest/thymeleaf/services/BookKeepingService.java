package jpa.rest.thymeleaf.services;

import jpa.rest.thymeleaf.DAO.model.Department;
import jpa.rest.thymeleaf.DAO.model.Payment;
import jpa.rest.thymeleaf.DAO.model.User;
import jpa.rest.thymeleaf.http.exceptions.handler.ResourceNotFoundException;
import org.springframework.stereotype.Service;

@Service
public class BookKeepingService {

    public String annualSalary(User user){

        if(null == user)
            throw new ResourceNotFoundException();

        int numberOfMonths = 12;
        float bonus = user.getPayment().getBonus();
        float pay = user.getPayment().getPay();
        float totalPay = pay * numberOfMonths;
        float totalBonus = bonus * numberOfMonths;
        float totalSalary = totalPay + totalBonus;

        return "Uzytkownik " + user.getUserName() +
                "\n\tWyplata: " + pay +
                "\n\tBonus: " + bonus +
                "\n\tRoczna wyplata: " + pay + " * " + numberOfMonths + " = " + totalPay +
                "\n\tRoczny bonus: " + bonus + " * " + numberOfMonths + " = " + totalBonus +
                "\n\tRoczna wyplata: " + totalPay + " + " + totalBonus + " = " + totalSalary;
    }

    public String monthlyTaxes(Department department, int percent){

        if(null == department)
            throw new ResourceNotFoundException();

        String result = "Department name: " + department.getDepartmentName();
        float sum = 0;
        float sumOneUser = 0;
        Payment payment = null;

        for (User u : department.getUsers()) {
            payment = u.getPayment();
            if(null != payment){
                sumOneUser = payment.getPay() + payment.getBonus();
                result += "\n" + u.getUserName() + ": Pay: " + payment.getPay() + "+ Bonus: " + payment.getBonus() + " = " + sumOneUser;
                sum += sumOneUser;
            }
        }

        return result + "\n" + "Podatek: (" + sum + " / 100) * " + percent + " = " + getTaxByAmountAndPercent(sum, percent);
    }

    private float getTaxByAmountAndPercent(float amount, int percent){
        return (amount / 100) * percent;
    }

}
