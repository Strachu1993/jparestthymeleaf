package jpa.rest.thymeleaf.DAO.model;

import lombok.*;

import javax.persistence.*;
import java.time.LocalDate;

@ToString
@EqualsAndHashCode
@Setter
@Getter
@NoArgsConstructor
@Entity
@Table(name = "Payment")
public class Payment {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Setter(AccessLevel.PROTECTED)
    private long id;

    private float pay;

    private float bonus;

    private LocalDate dateOfPayment;

    public static final Payment createPayment(float bonus, LocalDate dateOfPayment, float pay){
        Payment payment = new Payment();
        payment.setBonus(bonus);
        payment.setDateOfPayment(dateOfPayment);
        payment.setPay(pay);
        return payment;
    }

}