package jpa.rest.thymeleaf.services;

import jpa.rest.thymeleaf.DAO.model.Department;
import jpa.rest.thymeleaf.DAO.model.Payment;
import jpa.rest.thymeleaf.DAO.model.User;
import jpa.rest.thymeleaf.DAO.repository.DepartmentRepository;
import jpa.rest.thymeleaf.DAO.repository.PaymentRepository;
import jpa.rest.thymeleaf.DAO.repository.UserRepository;
import jpa.rest.thymeleaf.logger.MyLogger;
import lombok.AllArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDate;
import java.util.List;

@AllArgsConstructor(onConstructor = @__({@Autowired}))
@Service
public class JpaService {

    private final UserRepository userRepository;
    private final PaymentRepository paymentRepository;
    private final DepartmentRepository departmentRepository;
    private final MyLogger myLogger;

    public List<User> getUsers(){
        return userRepository.findAll(new Sort(Sort.Direction.ASC, "userName"));
    }

    public User findUserByName(String name){
        return userRepository.findByUserName(name);
    }

    public Department findDepartmentByName(String departmentName){
        return departmentRepository.findByDepartmentName(departmentName);
    }

    @Transactional
    public void addUserToDepartment(long userId, long departmentId){
        departmentRepository.findOne(departmentId).getUsers().add(userRepository.findOne(userId));
        //userRepository.findOne(userId).getDepartments().add(departmentRepository.findOne(departmentId));
    }

    @Transactional
    public void addPaymantToUser(long paymantId, long userId){
        userRepository.findOne(userId).setPayment(paymentRepository.findOne(paymantId));
    }

    public void createDepartment(String address, String departmentName, String description, String mainMail, String mainPhone, String mainWww){
        departmentRepository.save(Department.createDepartment(address, departmentName, description,mainMail, mainPhone, mainWww));
    }

    public void createPaymant(float bonus, LocalDate dateOfPayment, float pay){
        paymentRepository.save(Payment.createPayment(bonus, dateOfPayment, pay));
    }

    public void createUser(String desc, String firstName, String lastName, String pass, String userName){
        userRepository.save(User.createUser(desc, firstName, lastName, pass, userName));
    }

    public boolean editUser(long userId, User userData){
        User user = userRepository.findOne(userId);

        if(null != user){
            user.setDescription(userData.getDescription());
            user.setFirstName(userData.getFirstName());
            user.setLastName(userData.getLastName());
            user.setUserName(userData.getUserName());
            user.setPassword(userData.getPassword());
            user.setPayment(userData.getPayment());

            userRepository.save(user);
            return true;
        }
        return false;
    }

    @Transactional
    public void logDataFromDb() {

        myLogger.space();

        myLogger.text("Paymants: " + paymentRepository.count(), 1);
        paymentRepository.findAll().forEach((array) -> myLogger.text(array, 2));

        myLogger.space();

        myLogger.text("Users: " + userRepository.count(), 1);
        userRepository.findAll().forEach(array -> {
            myLogger.text(array + " - Depatments: " + array.getDepartments().size(), 2);
            array.getDepartments().forEach((list) -> myLogger.text(list, 3));
        });

        myLogger.space();

        myLogger.text("Depatments: " + departmentRepository.count(), 1);
        departmentRepository.findAll().forEach(array -> {
            myLogger.text(array + " - Users: " + array.getUsers().size(), 2);
            array.getUsers().forEach((list) -> myLogger.text(list, 3));
        });

        myLogger.space();
    }

}
