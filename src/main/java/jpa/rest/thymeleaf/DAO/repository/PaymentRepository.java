package jpa.rest.thymeleaf.DAO.repository;

import jpa.rest.thymeleaf.DAO.model.Payment;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface PaymentRepository extends JpaRepository<Payment, Long> {

}
