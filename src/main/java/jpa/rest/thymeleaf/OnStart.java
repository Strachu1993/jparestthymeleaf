package jpa.rest.thymeleaf;

import jpa.rest.thymeleaf.services.JpaService;
import lombok.AllArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import javax.annotation.PostConstruct;
import java.time.LocalDate;

@AllArgsConstructor(onConstructor = @__({@Autowired}))
@Component
public class OnStart {

    private final JpaService jpaService;
    private final long countUser = 6 ;

    @PostConstruct
    void onStart(){

        createPaymants();
        createUsers();
        createDepartments();
        addRelactionPaymantsToUsers();
        addRelactionUserToDepartment();

        jpaService.logDataFromDb();

    }

    private void addRelactionUserToDepartment() {
        for(int j=1 ; j<countUser+1 ; j++)
            for(int i=1 ; i<countUser+1 ; i++){
                jpaService.addUserToDepartment(i, j);
        }
    }

    private void addRelactionPaymantsToUsers() {
        for(int i=1 ; i<countUser+1 ; i++)
            if(i % 2 == 0)
                jpaService.addPaymantToUser(1,i);
            else
                jpaService.addPaymantToUser(2,i);
    }

    private void createDepartments() {
        for(int i=0 ; i<countUser ; i++)
            jpaService.createDepartment("Address: " + i, "Name: " + i, "description" + i, "wp.pl" + 1, "00202020"+i,"www"+i );
    }

    private void createUsers() {
        for(int i=0 ; i<countUser ; i++)
            jpaService.createUser("Example desc:" + i, "Example firstName:" + i, "Example lastName:" + i, "Example password:" + i, "ExampleUserName:" + i);
    }

    private void createPaymants() {
        jpaService.createPaymant(44.2f, LocalDate.now(), 5555.33f);
        jpaService.createPaymant(44.2f, LocalDate.now(), 5555.33f);
    }

}

