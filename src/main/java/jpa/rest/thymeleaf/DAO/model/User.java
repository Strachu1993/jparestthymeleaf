package jpa.rest.thymeleaf.DAO.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.sun.istack.internal.NotNull;
import lombok.*;
import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@ToString(exclude = {"departments"})
@Setter
@Getter
@NoArgsConstructor
@EqualsAndHashCode(exclude = {"departments"})
@Entity
@Table(name = "User")
public class User {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @NotNull
    @Setter(AccessLevel.PROTECTED)
    private long id;

    private String description;

    private String firstName;

    private String lastName;

    private String password;

    private String userName;

    @ManyToOne
    private Payment payment;

    @JsonIgnore
    @ManyToMany(mappedBy = "users", fetch=FetchType.EAGER)
    private List<Department> departments = new ArrayList<>();

    public static final User createUser(String desc, String firstName, String lastName, String pass, String userName){
        User user = new User();
        user.setDescription(desc);
        user.setFirstName(firstName);
        user.setLastName(lastName);
        user.setPassword(pass);
        user.setUserName(userName);
        return user;
    }

}